# LinkWeChat-DB

#### 介绍
LinkWeChat数据库脚本,此分支便于LinkWeChat相关数据库得脚本管理。

#### 版本介绍
     v1.0_init_db.sql 系统初始化脚本

# LinkWeChat-系统基础表
### sys_post(岗位信息表)
| 字段名 | 字段说明 | 字段类型 | 默认值 | 是否为空 |
|------ |------ |------ |------ |------ |
|post_id |岗位ID |bigint |null |不为空 |
|post_code |岗位编码 |varchar |null |不为空 |
|post_name |岗位名称 |varchar |null |不为空 |
|post_sort |显示顺序 |int |null |不为空 |
|status |状态（0正常 1停用） |char |null |不为空 |
|create_by |创建者 |varchar | |是 |
|create_time |创建时间 |datetime |null |是 |
|update_by |更新者 |varchar | |是 |
|update_time |更新时间 |datetime |null |是 |
|remark |备注 |varchar |null |是 |

### sys_role(角色信息表)
| 字段名 | 字段说明 | 字段类型 | 默认值 | 是否为空 |
|------ |------ |------ |------ |------ |
|role_id |角色ID |bigint |null |不为空 |
|role_name |角色名称 |varchar |null |不为空 |
|role_key |角色权限字符串 |varchar |null |不为空 |
|role_sort |显示顺序 |int |null |不为空 |
|data_scope |数据范围（1：全部数据权限 2：自定数据权限 3：本部门数据权限 4：本部门及以下数据权限） |char |1 |是 |
|status |角色状态（0正常 1停用） |char |null |不为空 |
|del_flag |删除标志（0代表存在 2代表删除） |char |0 |是 |
|create_by |创建者 |varchar | |是 |
|create_time |创建时间 |datetime |null |是 |
|update_by |更新者 |varchar | |是 |
|update_time |更新时间 |datetime |null |是 |
|remark |备注 |varchar |null |是 |

### sys_role_dept(角色和部门关联表)
| 字段名 | 字段说明 | 字段类型 | 默认值 | 是否为空 |
|------ |------ |------ |------ |------ |
|role_id |角色ID |bigint |null |不为空 |
|dept_id |部门ID |bigint |null |不为空 |

### sys_role_menu(角色和菜单关联表)
| 字段名 | 字段说明 | 字段类型 | 默认值 | 是否为空 |
|------ |------ |------ |------ |------ |
|role_id |角色ID |bigint |null |不为空 |
|menu_id |菜单ID |bigint |null |不为空 |

### sys_user(用户信息表)
| 字段名 | 字段说明 | 字段类型 | 默认值 | 是否为空 |
|------ |------ |------ |------ |------ |
|user_id |用户ID |bigint |null |不为空 |
|dept_id |部门ID |bigint |null |是 |
|user_name |用户账号 |varchar |null |不为空 |
|nick_name |用户昵称 |varchar |null |不为空 |
|user_type |用户类型（00系统用户）(11:企业微信用户) |varchar |00 |是 |
|email |用户邮箱 |varchar | |是 |
|phonenumber |手机号码 |varchar | |是 |
|sex |用户性别（0男 1女 2未知） |char |0 |是 |
|avatar |头像地址 |varchar | |是 |
|password |密码 |varchar | |是 |
|status |帐号状态（0正常 1停用） |char |0 |是 |
|del_flag |删除标志（0代表存在 2代表删除） |char |0 |是 |
|login_ip |最后登陆IP |varchar | |是 |
|login_date |最后登陆时间 |datetime |null |是 |
|create_by |创建者 |varchar | |是 |
|create_time |创建时间 |datetime |null |是 |
|update_by |更新者 |varchar | |是 |
|update_time |更新时间 |datetime |null |是 |
|remark |备注 |varchar |null |是 |
|corp_account | |varchar |null |是 |
|we_user_id | |varchar |null |是 |
|corp_id | |varchar |null |是 |

### sys_user_post(用户与岗位关联表)
| 字段名 | 字段说明 | 字段类型 | 默认值 | 是否为空 |
|------ |------ |------ |------ |------ |
|user_id |用户ID |bigint |null |不为空 |
|post_id |岗位ID |bigint |null |不为空 |

### sys_user_role(用户和角色关联表)
| 字段名 | 字段说明 | 字段类型 | 默认值 | 是否为空 |
|------ |------ |------ |------ |------ |
|user_id |用户ID |bigint |null |不为空 |
|role_id |角色ID |bigint |null |不为空 |


### sys_config(参数配置表)
| 字段名 | 字段说明 | 字段类型 | 默认值 | 是否为空 |
|------ |------ |------ |------ |------ |
|config_id |参数主键 |int |null |不为空 |
|config_name |参数名称 |varchar | |是 |
|config_key |参数键名 |varchar | |是 |
|config_value |参数键值 |varchar | |是 |
|config_type |系统内置（Y是 N否） |char |N |是 |
|create_by |创建者 |varchar | |是 |
|create_time |创建时间 |datetime |null |是 |
|update_by |更新者 |varchar | |是 |
|update_time |更新时间 |datetime |null |是 |
|remark |备注 |varchar |null |是 |

### sys_dept(部门表)
| 字段名 | 字段说明 | 字段类型 | 默认值 | 是否为空 |
|------ |------ |------ |------ |------ |
|dept_id |部门id |bigint |null |不为空 |
|parent_id |父部门id |bigint |0 |是 |
|ancestors |祖级列表 |varchar | |是 |
|dept_name |部门名称 |varchar | |是 |
|order_num |显示顺序 |int |0 |是 |
|leader |负责人 |varchar |null |是 |
|phone |联系电话 |varchar |null |是 |
|email |邮箱 |varchar |null |是 |
|status |部门状态（0正常 1停用） |char |0 |是 |
|del_flag |删除标志（0代表存在 2代表删除） |char |0 |是 |
|create_by |创建者 |varchar | |是 |
|create_time |创建时间 |datetime |null |是 |
|update_by |更新者 |varchar | |是 |
|update_time |更新时间 |datetime |null |是 |

### sys_dict_data(字典数据表)
| 字段名 | 字段说明 | 字段类型 | 默认值 | 是否为空 |
|------ |------ |------ |------ |------ |
|dict_code |字典编码 |bigint |null |不为空 |
|dict_sort |字典排序 |int |0 |是 |
|dict_label |字典标签 |varchar | |是 |
|dict_value |字典键值 |varchar | |是 |
|dict_type |字典类型 |varchar | |是 |
|css_class |样式属性（其他样式扩展） |varchar |null |是 |
|list_class |表格回显样式 |varchar |null |是 |
|is_default |是否默认（Y是 N否） |char |N |是 |
|status |状态（0正常 1停用） |char |0 |是 |
|create_by |创建者 |varchar | |是 |
|create_time |创建时间 |datetime |null |是 |
|update_by |更新者 |varchar | |是 |
|update_time |更新时间 |datetime |null |是 |
|remark |备注 |varchar |null |是 |

### sys_dict_type(字典类型表)
| 字段名 | 字段说明 | 字段类型 | 默认值 | 是否为空 |
|------ |------ |------ |------ |------ |
|dict_id |字典主键 |bigint |null |不为空 |
|dict_name |字典名称 |varchar | |是 |
|dict_type |字典类型 |varchar | |是 |
|status |状态（0正常 1停用） |char |0 |是 |
|create_by |创建者 |varchar | |是 |
|create_time |创建时间 |datetime |null |是 |
|update_by |更新者 |varchar | |是 |
|update_time |更新时间 |datetime |null |是 |
|remark |备注 |varchar |null |是 |

### sys_job(定时任务调度表)
| 字段名 | 字段说明 | 字段类型 | 默认值 | 是否为空 |
|------ |------ |------ |------ |------ |
|job_id |任务ID |bigint |null |不为空 |
|job_name |任务名称 |varchar | |不为空 |
|job_group |任务组名 |varchar |DEFAULT |不为空 |
|invoke_target |调用目标字符串 |varchar |null |不为空 |
|cron_expression |cron执行表达式 |varchar | |是 |
|misfire_policy |计划执行错误策略（1立即执行 2执行一次 3放弃执行） |varchar |3 |是 |
|concurrent |是否并发执行（0允许 1禁止） |char |1 |是 |
|status |状态（0正常 1暂停） |char |0 |是 |
|create_by |创建者 |varchar | |是 |
|create_time |创建时间 |datetime |null |是 |
|update_by |更新者 |varchar | |是 |
|update_time |更新时间 |datetime |null |是 |
|remark |备注信息 |varchar | |是 |

### sys_job_log(定时任务调度日志表)
| 字段名 | 字段说明 | 字段类型 | 默认值 | 是否为空 |
|------ |------ |------ |------ |------ |
|job_log_id |任务日志ID |bigint |null |不为空 |
|job_name |任务名称 |varchar |null |不为空 |
|job_group |任务组名 |varchar |null |不为空 |
|invoke_target |调用目标字符串 |varchar |null |不为空 |
|job_message |日志信息 |varchar |null |是 |
|status |执行状态（0正常 1失败） |char |0 |是 |
|exception_info |异常信息 |varchar | |是 |
|create_time |创建时间 |datetime |null |是 |

### sys_logininfor(系统访问记录)
| 字段名 | 字段说明 | 字段类型 | 默认值 | 是否为空 |
|------ |------ |------ |------ |------ |
|info_id |访问ID |bigint |null |不为空 |
|user_name |用户账号 |varchar | |是 |
|ipaddr |登录IP地址 |varchar | |是 |
|login_location |登录地点 |varchar | |是 |
|browser |浏览器类型 |varchar | |是 |
|os |操作系统 |varchar | |是 |
|status |登录状态（0成功 1失败） |char |0 |是 |
|msg |提示消息 |varchar | |是 |
|login_time |访问时间 |datetime |null |是 |

### sys_menu(菜单权限表)
| 字段名 | 字段说明 | 字段类型 | 默认值 | 是否为空 |
|------ |------ |------ |------ |------ |
|menu_id |菜单ID |bigint |null |不为空 |
|menu_name |菜单名称 |varchar |null |不为空 |
|parent_id |父菜单ID |bigint |0 |是 |
|order_num |显示顺序 |int |0 |是 |
|path |路由地址 |varchar | |是 |
|component |组件路径 |varchar |null |是 |
|is_frame |是否为外链（0是 1否） |int |1 |是 |
|menu_type |菜单类型（M目录 C菜单 F按钮） |char | |是 |
|visible |菜单状态（0显示 1隐藏） |char |0 |是 |
|status |菜单状态（0正常 1停用） |char |0 |是 |
|perms |权限标识 |varchar |null |是 |
|icon |菜单图标 |varchar |# |是 |
|create_by |创建者 |varchar | |是 |
|create_time |创建时间 |datetime |null |是 |
|update_by |更新者 |varchar | |是 |
|update_time |更新时间 |datetime |null |是 |
|remark |备注 |varchar | |是 |

### sys_notice(通知公告表)
| 字段名 | 字段说明 | 字段类型 | 默认值 | 是否为空 |
|------ |------ |------ |------ |------ |
|notice_id |公告ID |int |null |不为空 |
|notice_title |公告标题 |varchar |null |不为空 |
|notice_type |公告类型（1通知 2公告） |char |null |不为空 |
|notice_content |公告内容 |varchar |null |是 |
|status |公告状态（0正常 1关闭） |char |0 |是 |
|create_by |创建者 |varchar | |是 |
|create_time |创建时间 |datetime |null |是 |
|update_by |更新者 |varchar | |是 |
|update_time |更新时间 |datetime |null |是 |
|remark |备注 |varchar |null |是 |

### sys_oper_log(操作日志记录表)
| 字段名 | 字段说明 | 字段类型 | 默认值 | 是否为空 |
|------ |------ |------ |------ |------ |
|oper_id |日志主键 |bigint |null |不为空 |
|title |模块标题 |varchar | |是 |
|business_type |业务类型（0其它 1新增 2修改 3删除） |int |0 |是 |
|method |方法名称 |varchar | |是 |
|request_method |请求方式 |varchar | |是 |
|operator_type |操作类别（0其它 1后台用户 2手机端用户） |int |0 |是 |
|oper_name |操作人员 |varchar | |是 |
|dept_name |部门名称 |varchar | |是 |
|oper_url |请求URL |varchar | |是 |
|oper_ip |主机地址 |varchar | |是 |
|oper_location |操作地点 |varchar | |是 |
|oper_param |请求参数 |varchar | |是 |
|json_result |返回参数 |varchar | |是 |
|status |操作状态（0正常 1异常） |int |0 |是 |
|error_msg |错误消息 |varchar | |是 |
|oper_time |操作时间 |datetime |null |是 |


### gen_table(代码生成业务表)
| 字段名 | 字段说明 | 字段类型 | 默认值 | 是否为空 |
|------ |------ |------ |------ |------ |
|table_id |编号 |bigint |null |不为空 |
|table_name |表名称 |varchar | |是 |
|table_comment |表描述 |varchar | |是 |
|class_name |实体类名称 |varchar | |是 |
|tpl_category |使用的模板（crud单表操作 tree树表操作） |varchar |crud |是 |
|package_name |生成包路径 |varchar |null |是 |
|module_name |生成模块名 |varchar |null |是 |
|business_name |生成业务名 |varchar |null |是 |
|function_name |生成功能名 |varchar |null |是 |
|function_author |生成功能作者 |varchar |null |是 |
|gen_type |生成代码方式（0zip压缩包 1自定义路径） |char |0 |是 |
|gen_path |生成路径（不填默认项目路径） |varchar |/ |是 |
|options |其它生成选项 |varchar |null |是 |
|create_by |创建者 |varchar | |是 |
|create_time |创建时间 |datetime |null |是 |
|update_by |更新者 |varchar | |是 |
|update_time |更新时间 |datetime |null |是 |
|remark |备注 |varchar |null |是 |

### gen_table_column(代码生成业务表字段表)
| 字段名 | 字段说明 | 字段类型 | 默认值 | 是否为空 |
|------ |------ |------ |------ |------ |
|column_id |编号 |bigint |null |不为空 |
|table_id |归属表编号 |varchar |null |是 |
|column_name |列名称 |varchar |null |是 |
|column_comment |列描述 |varchar |null |是 |
|column_type |列类型 |varchar |null |是 |
|java_type |JAVA类型 |varchar |null |是 |
|java_field |JAVA字段名 |varchar |null |是 |
|is_pk |是否主键（1是） |char |null |是 |
|is_increment |是否自增（1是） |char |null |是 |
|is_required |是否必填（1是） |char |null |是 |
|is_insert |是否为插入字段（1是） |char |null |是 |
|is_edit |是否编辑字段（1是） |char |null |是 |
|is_list |是否列表字段（1是） |char |null |是 |
|is_query |是否查询字段（1是） |char |null |是 |
|query_type |查询方式（等于、不等于、大于、小于、范围） |varchar |EQ |是 |
|html_type |显示类型（文本框、文本域、下拉框、复选框、单选框、日期控件） |varchar |null |是 |
|dict_type |字典类型 |varchar | |是 |
|sort |排序 |int |null |是 |
|create_by |创建者 |varchar | |是 |
|create_time |创建时间 |datetime |null |是 |
|update_by |更新者 |varchar | |是 |
|update_time |更新时间 |datetime |null |是 |




# LinkWeChat-企业微信相关表

#### we_corp_account(企业微信参数配置表)
    
| 字段名 | 字段说明 | 字段类型 | 默认值 | 是否为空 |
|------ |------ |------ |------ |------ |
|id |id |bigint |null |不为空 |
|company_name |企业名称 |varchar |null |是 |
|corp_id |企业ID |varchar |null |是 |
|corp_secret |应用的密钥凭证 |varchar |null |是 |
|contact_secret |外部联系人密钥 |varchar |null |是 |
|wx_qr_login_redirect_uri |企业微信扫码登陆系统回调地址 |varchar |null |是 |
|provider_secret | |varchar |null |是 |
|chat_secret |会话存档密钥 |varchar |null |是 |
|agent_id |应用id |varchar |null |是 |
|agent_secret |应用密钥 |varchar |null |是 |
|status |帐号状态（0正常 1停用) |char |1 |是 |
|del_flag |删除标志（0代表存在 2代表删除） |char |0 |是 |
|create_by | |varchar |null |是 |
|create_time |创建时间 |datetime |null |是 |
|update_by | |varchar |null |是 |
|update_time |更新时间 |datetime |null |是 |
|customer_churn_notice_switch |客户流失通知开关 0:关闭 1:开启 |char |0 |是 |
|corp_account |企业管理员账号 |varchar |null |是 |
|token | |varchar |null |是 |
|encoding_aes_key | |varchar |null |是 |
|app_id | |varchar |null |是 |
|secret | |varchar |null |是 |
|fission_url | |varchar |null |是 |
|fission_group_url | |varchar |null |是 |
|finance_private_key |会话存档私钥 |text |null |是 |
|h5_do_main_name |H5域名 |varchar |null |是 |
|corp_type |账号类型(1:超管体系公司账号;2:普通下级公司体系账号) |tinyint |2 |是 |

#### we_department(企业微信组织架构表)

| 字段名 | 字段说明 | 字段类型 | 默认值 | 是否为空 |
|------ |------ |------ |------ |------ |
|id |id |bigint |null |不为空 |
|name |部门名称 |varchar |null |不为空 |
|parent_id | |bigint |null |不为空 |

#### we_user(企业微信通讯录员工表)

| 字段名 | 字段说明 | 字段类型 | 默认值 | 是否为空 |
|------ |------ |------ |------ |------ |
|user_id | |varchar |null |不为空 |
|head_image_url |头像地址 |varchar |null |是 |
|user_name |用户名称 |varchar |null |是 |
|alias |用户昵称 |varchar |null |是 |
|gender |性别。1表示男性，2表示女性 |tinyint |1 |是 |
|mobile |手机号 |varchar |null |是 |
|email |邮箱 |varchar |null |是 |
|wx_account |个人微信号 |varchar |null |是 |
|department |用户所属部门,使用逗号隔开,字符串格式存储 |tinytext |null |是 |
|position |职务 |varchar |null |是 |
|is_leader_in_dept |1表示为上级,0表示普通成员(非上级)。 |tinytext |null |是 |
|join_time |入职时间 |date |null |是 |
|enable |是否启用(1表示启用成员，0表示禁用成员) |tinyint |1 |是 |
|id_card |身份证号 |char |null |是 |
|qq_account |QQ号 |varchar |null |是 |
|telephone |座机 |varchar |null |是 |
|address |地址 |varchar |null |是 |
|birthday |生日 |date |null |是 |
|remark |备注 |varchar |null |是 |
|customer_tags |客户标签,字符串使用逗号隔开 |varchar |null |是 |
|dimission_time |离职时间 |datetime |null |是 |
|is_allocate |离职是否分配(1:已分配;0:未分配;) |tinyint |0 |是 |
|is_activate |激活状态: 1=已激活，2=已禁用，4=未激活，5=退出企业,6=删除 |tinyint |null |是 |
|isOpenChat |是否开启会话存档 0：关闭 1：开启 |tinyint |0 |是 |
|create_by | |varchar |null |是 |
|update_by | |varchar |null |是 |
|create_time |创建时间 |datetime |null |是 |
|update_time |更新时间 |datetime |null |是 |

#### we_user_behavior_data(联系客户统计数据表)

| 字段名 | 字段说明 | 字段类型 | 默认值 | 是否为空 |
|------ |------ |------ |------ |------ |
|id |id |bigint |null |不为空 |
|user_id |客户id |varchar |null |是 |
|stat_time |数据日期，为当日0点的时间戳 |datetime |null |是 |
|new_apply_cnt |发起申请数 |int |null |是 |
|new_contact_cnt |新增客户数，成员新添加的客户数量 |int |null |是 |
|chat_cnt |聊天总数， 成员有主动发送过消息的单聊总数 |int |null |是 |
|message_cnt |发送消息数，成员在单聊中发送的消息总数 |int |null |是 |
|reply_percentage |已回复聊天占比，浮点型，客户主动发起聊天后，成员在一个自然日内有回复过消息的聊天数/客户主动发起的聊天数比例，不包括群聊，仅在确有聊天时返回 |double |null |是 |
|avg_reply_time |平均首次回复时长 |int |null |是 |
|negative_feedback_cnt |删除/拉黑成员的客户数，即将成员删除或加入黑名单的客户数 |int |null |是 |


#### we_tag_group(标签组表)

| 字段名 | 字段说明 | 字段类型 | 默认值 | 是否为空 |
|------ |------ |------ |------ |------ |
|group_id | |varchar |null |不为空 |
|gourp_name | |varchar |null |是 |
|create_by | |varchar |null |是 |
|create_time |创建时间 |datetime |null |是 |
|status |帐号状态（0正常 2删除） |char |0 |是 |

#### we_tag(企业微信标签表)

| 字段名 | 字段说明 | 字段类型 | 默认值 | 是否为空 |
|------ |------ |------ |------ |------ |
|group_id |标签组id |varchar |null |不为空 |
|name |标签名 |varchar |null |是 |
|create_time |创建时间 |datetime |null |是 |
|status |状态（0正常 1删除） |char |0 |是 |
|tag_id |微信端返回的id |varchar |null |不为空 |


### we_customer(企业微信客户表)
| 字段名 | 字段说明 | 字段类型 | 默认值 | 是否为空 |
|------ |------ |------ |------ |------ |
|corp_id |企业id |varchar |null |是 |
|external_userid |外部联系人的userid |varchar |null |不为空 |
|name |外部联系人名称 |varchar |null |是 |
|avatar |外部联系人头像 |varchar |null |是 |
|type |外部联系人的类型，1表示该外部联系人是微信用户，2表示该外部联系人是企业微信用户 |tinyint |null |是 |
|gender |外部联系人性别 0-未知 1-男性 2-女性 |tinyint |0 |不为空 |
|unionid |外部联系人在微信开放平台的唯一身份标识,通过此字段企业可将外部联系人与公众号/小程序用户关联起来。 |varchar |null |是 |
|birthday |生日 |datetime |null |是 |
|corp_name |客户企业简称 |varchar |null |是 |
|corp_full_name |客户企业全称 |varchar |null |是 |
|position |客户职位 |varchar |null |是 |
|is_open_chat |是否开启会话存档 0：关闭 1：开启 |tinyint |0 |是 |
|create_time |创建时间 |datetime |null |是 |
|update_time |更新时间 |datetime |null |是 |
|create_by | |varchar |null |是 |
|update_by | |varchar |null |是 |
|del_flag |删除标识 0 有效 2 删除 |tinyint |0 |不为空 |

### we_flower_customer_rel(具有外部联系人功能企业员工也客户的关系表)
| 字段名 | 字段说明 | 字段类型 | 默认值 | 是否为空 |
|------ |------ |------ |------ |------ |
|id |id |bigint |null |不为空 |
|user_id |添加了此外部联系人的企业成员userid |varchar |null |是 |
|remark |该成员对此外部联系人的备注 |varchar |null |是 |
|description |该成员对此外部联系人的描述 |tinytext |null |是 |
|create_time |创建时间 |datetime |null |是 |
|remark_corp_name |该成员对此客户备注的企业名称
 |varchar |null |是 |
|remark_mobiles |该成员对此客户备注的手机号码 |varchar |null |是 |
|oper_userid |发起添加的userid，如果成员主动添加，为成员的userid；如果是客户主动添加，则为客户的外部联系人userid；如果是内部成员共享/管理员分配，则为对应的成员/管理员userid |varchar |null |是 |
|qq |客户QQ |varchar |null |是 |
|address |客户地址 |varchar |null |是 |
|email |邮件 |varchar |null |是 |
|add_way |该成员添加此客户的来源， |varchar |null |是 |
|state |企业自定义的state参数，用于区分客户具体是通过哪个「联系我」添加，由企业通过创建「联系我」方式指定 |varchar |null |是 |
|external_userid |客户id |varchar |null |是 |
|status |状态（0正常 1删除） |char |0 |是 |

### we_flower_customer_tag_rel(客户标签关系表)
| 字段名 | 字段说明 | 字段类型 | 默认值 | 是否为空 |
|------ |------ |------ |------ |------ |
|id |id |bigint |null |不为空 |
|external_userid |客户id |varchar |null |是 |
|flower_customer_rel_id |添加客户的企业微信用户 |bigint |null |不为空 |
|tag_id |标签id |varchar |null |不为空 |
|create_time |创建时间 |datetime |null |是 |

### we_customer_message(群发消息|微信消息表)
| 字段名 | 字段说明 | 字段类型 | 默认值 | 是否为空 |
|------ |------ |------ |------ |------ |
|message_id | |bigint |null |不为空 |
|original_id |原始数据表id |bigint |null |是 |
|chat_type |群发任务的类型，默认为single，表示发送给客户，group表示发送给客户群 |varchar |null |是 |
|external_userid |客户的外部联系人id列表JSON |text |null |是 |
|sender |发送企业群发消息的成员userid，当类型为发送给客户群时必填(和企微客户沟通后确认是群主id) |varchar |null |是 |
|check_status |消息发送状态 0 未发送  1 已发送 |varchar |null |是 |
|msgid |企业群发消息的id，可用于<a href="https://work.weixin.qq.com/api/doc/90000/90135/92136">获取群发消息发送结果</a> |varchar |null |是 |
|content |消息内容 |text |null |是 |
|create_by | |varchar |null |是 |
|create_time |创建时间 |datetime |null |是 |
|update_by | |varchar |null |是 |
|update_time |更新时间 |datetime |null |是 |
|del_flag | |int |null |是 |
|setting_time |发送时间 |varchar |null |是 |
|expect_send |预计发送消息数（客户对应多少人 客户群对应多个群） |int |0 |是 |
|actual_send |实际发送消息数（客户对应多少人 客户群对应多个群） |int |0 |是 |
|timed_task |是否定时任务 0 常规 1 定时发送 |int |0 |是 |

### we_customer_messageOriginal(群发消息|原始数据信息表)
| 字段名 | 字段说明 | 字段类型 | 默认值 | 是否为空 |
|------ |------ |------ |------ |------ |
|message_original_Id | |bigint |null |不为空 |
|push_type |群发类型 0 发给客户 1 发给客户群 |varchar |null |是 |
|message_type |0 文本消息  1 图片消息 2 链接消息   3 小程序消息 |varchar |null |是 |
|staff_id |员工id |text |null |是 |
|tag |客户标签id列表 |varchar |null |是 |
|group |群组名称id |varchar |null |是 |
|department |部门id |varchar |null |是 |
|push_range |消息范围 0 全部客户  1 指定客户 |varchar |null |是 |
|create_by | |varchar |null |是 |
|create_time |创建时间 |datetime |null |是 |
|update_by | |varchar |null |是 |
|update_time |更新时间 |datetime |null |是 |
|del_flag | |int |0 |是 |

### we_customer_messageTimeTask(信息定时发送表)
| 字段名 | 字段说明 | 字段类型 | 默认值 | 是否为空 |
|------ |------ |------ |------ |------ |
|task_id |任务id |bigint |null |不为空 |
|message_id |消息id |bigint |null |不为空 |
|message_info |消息原始信息 |longtext |null |不为空 |
|customers_info |客户信息列表 |longtext |null |是 |
|groups_info |客户群组信息列表 |longtext |null |是 |
|setting_time |定时时间的毫秒数 |bigint |null |不为空 |
|create_by | |varchar |null |是 |
|create_time |创建时间 |datetime |null |是 |
|update_by | |varchar |null |是 |
|update_time |更新时间 |datetime |null |是 |
|solved |0 未解决 1 已解决 |int |0 |是 |
|del_flag | |int |null |是 |

### we_customer_messgaeResult(群发消息|微信消息发送结果)
| 字段名 | 字段说明 | 字段类型 | 默认值 | 是否为空 |
|------ |------ |------ |------ |------ |
|messgae_result_id | |bigint |null |不为空 |
|message_id |微信消息表id |bigint |null |是 |
|external_userid |外部联系人userid |varchar |null |是 |
|chat_id |外部客户群id |varchar |null |是 |
|userid |企业服务人员的userid |varchar |null |是 |
|status |发送状态 0-未发送 1-已发送 2-因客户不是好友导致发送失败 3-因客户已经收到其他群发消息导致发送失败 |varchar |null |是 |
|send_time | |varchar |null |是 |
|external_name |外部联系人名称 |varchar |null |是 |
|user_name |企业服务人员的名称 |varchar |null |是 |
|send_type |0 发给客户 1 发给客户群 2 定时发送 |varchar |null |是 |
|setting_time |定时发送时间 |varchar |null |是 |
|create_by | |varchar |null |是 |
|create_time |创建时间 |datetime |null |是 |
|update_by | |varchar |null |是 |
|update_time |更新时间 |datetime |null |是 |
|del_flag | |int |0 |是 |
|chat_name |外部客户群名称 |varchar |null |是 |

### we_customer_seedMessage(群发消息|子消息表(包括 文本消息、图片消息、链接消息、小程序消息))
| 字段名 | 字段说明 | 字段类型 | 默认值 | 是否为空 |
|------ |------ |------ |------ |------ |
|id |id |bigint |null |是 |
|message_id |微信消息表id |bigint |null |是 |
|content |消息文本内容，最多4000个字节 |text |null |是 |
|media_id |图片消息：图片的media_id，可以通过 <a href="https://work.weixin.qq.com/api/doc/90000/90135/90253">素材管理接口</a>获得 |varchar |null |是 |
|pic_url |图片消息：图片的链接，仅可使用<a href="https://work.weixin.qq.com/api/doc/90000/90135/90256">上传图片接口</a>得到的链接 |varchar |null |是 |
|link_title |链接消息：图文消息标题 |varchar |null |是 |
|link_picurl |链接消息：图文消息封面的url |varchar |null |是 |
|lin_desc |链接消息：图文消息的描述，最多512个字节 |varchar |null |是 |
|link_url |链接消息：图文消息的链接 |varchar |null |是 |
|miniprogram_title |小程序消息标题，最多64个字节 |varchar |null |是 |
|miniprogram_media_id |小程序消息封面的mediaid，封面图建议尺寸为520*416 |varchar |null |是 |
|appid |小程序appid，必须是关联到企业的小程序应用 |varchar |null |是 |
|page |小程序page路径 |varchar |null |是 |
|create_by | |varchar |null |是 |
|create_time |创建时间 |datetime |null |是 |
|update_by | |varchar |null |是 |
|update_time |更新时间 |datetime |null |是 |
|del_flag | |int |0 |是 |
|seed_message_id | |bigint |null |不为空 |

### we_customer_trajectory(客户轨迹表)
| 字段名 | 字段说明 | 字段类型 | 默认值 | 是否为空 |
|------ |------ |------ |------ |------ |
|id |id |bigint |null |不为空 |
|trajectory_type |轨迹类型(1:信息动态;2:社交动态;3:活动规则;4:待办动态) |tinyint |null |是 |
|external_userid |外部联系人id |varchar |null |是 |
|content |文案内容 |varchar |null |是 |
|create_date |处理日期 |date |null |是 |
|start_time |处理开始时间,开始时间 |time |null |是 |
|end_time |处理结束时间,结束时间开始段 |time |null |是 |
|create_time |创建时间 |datetime |null |是 |
|status |0:正常;1:完成;2:删除 |char |0 |是 |
|user_id |员工id |varchar |null |是 |
|agent_id | |bigint |null |是 |

### we_emple_code(员工活码表)
| 字段名 | 字段说明 | 字段类型 | 默认值 | 是否为空 |
|------ |------ |------ |------ |------ |
|id |id |bigint |null |不为空 |
|code_type |活码类型:1:批量;2:单人;3:多人; |tinyint |null |是 |
|skip_verify |客户添加时无需经过确认自动成为好友:1:是;0:否 |tinyint |1 |不为空 |
|scenario |活动场景 |varchar |null |是 |
|welcome_msg |欢迎语 |varchar |null |是 |
|create_by |创建人 |varchar |null |是 |
|create_time |创建时间 |datetime |null |是 |
|del_flag |0:正常;1:删除; |tinyint |null |是 |
|config_id |新增联系方式的配置id |varchar |null |是 |
|qr_code |二维码链接 |varchar |null |是 |
|media_id |素材id |bigint |null |是 |
|update_by |更新者 |varchar |null |是 |
|update_time |更新时间 |datetime |null |是 |
|scan_times |该员工活码的扫码次数 |int |0 |不为空 |
|is_join_confirm_friends | 客户添加时是否需要经过确认自动成为好友  1:是  0:否 |tinyint |0 |是 |
|state |用于区分客户具体是通过哪个「联系我」添加。不能超过30个字符 |varchar | |是 |

### we_emple_code_tag(员工活码标签表)
| 字段名 | 字段说明 | 字段类型 | 默认值 | 是否为空 |
|------ |------ |------ |------ |------ |
|id |id |bigint |null |不为空 |
|tag_id |标签id |varchar |null |是 |
|tag_name |标签名称 |varchar |null |是 |
|emple_code_id |员工活码id |bigint |null |是 |
|del_flag |0:正常;2:删除; |tinyint |0 |是 |

### we_emple_code_use_scop(员工活码使用人表)
| 字段名 | 字段说明 | 字段类型 | 默认值 | 是否为空 |
|------ |------ |------ |------ |------ |
|id |id |bigint |null |不为空 |
|emple_code_id |员工活码id |bigint |null |是 |
|business_id_type |业务id类型1:组织机构id,2:成员id |tinyint |null |是 |
|business_name |活码下使用人姓名 |varchar |null |是 |
|business_id |活码类型下业务使用人的id |varchar |null |是 |
|del_flag |0:正常;2:删除; |tinyint |null |是 |
|party_id |部门id列表，只在多人时有效 |bigint |null |是 |

### we_group(企业微信群表)
| 字段名 | 字段说明 | 字段类型 | 默认值 | 是否为空 |
|------ |------ |------ |------ |------ |
|chat_id | |varchar |null |不为空 |
|group_name |群名 |varchar |群聊 |是 |
|create_time |创建时间 |timestamp |null |是 |
|notice |群公告 |text |null |是 |
|owner |群主userId |varchar |null |是 |
|status |0 - 正常;1 - 跟进人离职;2 - 离职继承中;3 - 离职继承完成 |tinyint |0 |是 |

### we_group_code(客户群活码表)
| 字段名 | 字段说明 | 字段类型 | 默认值 | 是否为空 |
|------ |------ |------ |------ |------ |
|id |id |bigint |null |不为空 |
|code_url |二维码链接 |varchar |null |不为空 |
|avatar_url |二维码中心头像url |varchar |null |是 |
|activity_name |活码名称 |varchar |null |是 |
|activity_desc |活码描述 |varchar |null |是 |
|activity_scene |场景 |varchar |null |是 |
|guide |引导语 |varchar |null |是 |
|show_tip |是否进群提示:1:是;0:否; |tinyint |null |是 |
|tip_msg |进群提示语 |varchar |null |是 |
|customer_server_qr_code |客服二维码 |varchar |null |是 |
|del_flag |0:正常;2:删除; |tinyint |null |是 |
|create_by |创建者 |varchar | |是 |
|create_time |创建时间 |datetime |null |是 |
|update_by |更新者 |varchar | |是 |
|update_time |更新时间 |datetime |null |是 |
|remark |备注 |varchar |null |是 |

### we_group_code_actual(实际群码表)
| 字段名 | 字段说明 | 字段类型 | 默认值 | 是否为空 |
|------ |------ |------ |------ |------ |
|id |id |bigint |null |不为空 |
|actual_group_qr_code |实际群码 |varchar |null |是 |
|group_name |群名称 |varchar |null |是 |
|effect_time |有效期 |datetime |null |是 |
|scan_code_times_limit |扫码次数限制 |int |null |是 |
|group_code_id |群活码id |bigint |null |是 |
|chat_id |群聊id |varchar |null |是 |
|chat_group_name |群聊名称 |varchar |null |是 |
|scan_code_times |扫码次数 |int |0 |是 |
|del_flag |0:正常使用;2:删除; |tinyint |null |是 |
|status |0:使用中 |tinyint |0 |是 |
|create_by |创建者 |varchar |null |是 |
|create_time |创建时间 |datetime |null |是 |
|update_by |更新者 |varchar |null |是 |
|update_time |更新时间 |datetime |null |是 |

### we_group_member(企业微信群成员表)
| 字段名 | 字段说明 | 字段类型 | 默认值 | 是否为空 |
|------ |------ |------ |------ |------ |
|id |id |bigint |null |不为空 |
|user_id |群成员id |varchar |null |是 |
|chat_id |群id |varchar |null |是 |
|union_id |外部联系人在微信开放平台的唯一身份标识 |varchar |null |是 |
|join_time |加群时间 |timestamp |null |是 |
|join_scene |加入方式 |tinyint |null |是 |
|type |成员类型:1 - 企业成员;2 - 外部联系人 |tinyint |null |是 |

### we_group_sop(群SOP规则表)
| 字段名 | 字段说明 | 字段类型 | 默认值 | 是否为空 |
|------ |------ |------ |------ |------ |
|rule_id |群SOP主键 |bigint |null |不为空 |
|rule_name |规则名称 |varchar |null |不为空 |
|title |标题 |varchar |null |不为空 |
|content |内容 |varchar |null |不为空 |
|start_time |开始执行时间 |datetime |null |不为空 |
|end_time |结束时间 |datetime |null |不为空 |
|create_by |创建者 |varchar | |是 |
|create_time |创建时间 |datetime |null |是 |
|update_by |更新者 |varchar | |是 |
|update_time |更新时间 |datetime |null |是 |
|remark |备注 |varchar |null |是 |
|del_flag |逻辑删除字段， 0:未删除 1:已删除 |tinyint |0 |是 |

### we_group_sop_chat(SOP规则 - 群聊 关联表)
| 字段名 | 字段说明 | 字段类型 | 默认值 | 是否为空 |
|------ |------ |------ |------ |------ |
|rule_id |SOP id |bigint |null |不为空 |
|chat_id |群聊id |varchar |null |不为空 |
|is_done |规则是否已发送0：未发送 1：已发送 |smallint |null |不为空 |

### we_group_sop_material(群SOP规则 - 素材关联表)
| 字段名 | 字段说明 | 字段类型 | 默认值 | 是否为空 |
|------ |------ |------ |------ |------ |
|rule_id |SOP规则id |bigint |null |不为空 |
|material_id |素材id |bigint |null |不为空 |

### we_group_sop_pic(群SOP规则图片)
| 字段名 | 字段说明 | 字段类型 | 默认值 | 是否为空 |
|------ |------ |------ |------ |------ |
|rule_id |群SOP规则ID |bigint |null |不为空 |
|pic_url |图片URL |varchar |null |不为空 |

### we_group_statistic(群聊数据统计数据)
| 字段名 | 字段说明 | 字段类型 | 默认值 | 是否为空 |
|------ |------ |------ |------ |------ |
|id |id |bigint |null |不为空 |
|chat_id |群ID |varchar |null |是 |
|stat_time |数据日期 |datetime |null |不为空 |
|new_chat_cnt |新增客户群数量 |int |null |是 |
|chat_total |截至当天客户群总数量 |int |null |是 |
|chat_has_msg |截至当天有发过消息的客户群数量 |int |null |是 |
|new_member_cnt |客户群新增群人数 |int |null |是 |
|member_total |截至当天客户群总人数 |int |null |是 |
|member_has_msg |截至当天有发过消息的群成员数 |int |null |是 |
|msg_total |截至当天客户群消息总数 |int |null |是 |

### we_community_new_group(新客自动拉群)
| 字段名 | 字段说明 | 字段类型 | 默认值 | 是否为空 |
|------ |------ |------ |------ |------ |
|id |id |bigint |null |不为空 |
|empl_code_name |员工活码名称 |varchar |null |不为空 |
|group_code_id |群活码ID |bigint |null |不为空 |
|create_by |创建人 |varchar |null |是 |
|create_time |创建时间 |datetime |null |是 |
|update_by |修改人 |varchar |null |是 |
|update_time |更新时间 |datetime |null |是 |
|del_flag |0:正常;1:删除; |int |0 |是 |
|empl_code_id |员工活码id |bigint |null |不为空 |
|new_group_id | |bigint |null |是 |

### we_config(系统设置表)
| 字段名 | 字段说明 | 字段类型 | 默认值 | 是否为空 |
|------ |------ |------ |------ |------ |
|id |id |bigint |null |不为空 |
|name |名称 |varchar |null |不为空 |
|status |状态 1:开启 0:关闭 |char |0 |是 |
|type |类型 |int |null |不为空 |

### we_keyword_group(关键词拉群任务表)
| 字段名 | 字段说明 | 字段类型 | 默认值 | 是否为空 |
|------ |------ |------ |------ |------ |
|task_id |关键词拉群任务主键 |bigint |null |不为空 |
|task_name |任务名称 |varchar |null |不为空 |
|group_code_id |群活码id |bigint |null |不为空 |
|welcome_msg |加群欢迎语 |varchar |null |不为空 |
|create_by |创建者 |varchar | |是 |
|create_time |创建时间 |datetime |null |是 |
|update_by |更新者 |varchar | |是 |
|update_time |更新时间 |datetime |null |是 |
|remark |备注 |varchar | |是 |
|del_flag |删除标记 0 未删除 1已删除 |char |0 |不为空 |
|keywords |关键词，以逗号分隔 |varchar |null |不为空 |

### we_material(素材表)
| 字段名 | 字段说明 | 字段类型 | 默认值 | 是否为空 |
|------ |------ |------ |------ |------ |
|id |id |bigint |null |不为空 |
|category_id |分类id |bigint |null |是 |
|material_url |本地资源文件地址 |varchar |null |是 |
|content |文本内容、图片文案 |text |null |是 |
|material_name |图片名称 |varchar |null |是 |
|digest |摘要 |varchar |null |是 |
|create_by | |varchar |null |是 |
|create_time |创建时间 |datetime |null |是 |
|update_by | |varchar |null |是 |
|update_time |更新时间 |datetime |null |是 |
|cover_url |封面本地资源文件 |varchar |null |是 |
|del_flag |0 未删除 1 已删除 |int |0 |是 |
|audio_time |音频时长 |varchar |0 |是 |

### we_message_push(消息发送的表)
| 字段名 | 字段说明 | 字段类型 | 默认值 | 是否为空 |
|------ |------ |------ |------ |------ |
|message_push_id |主键id |bigint |null |不为空 |
|push_type |群发类型 0 发给客户 1 发给客户群 |int |0 |是 |
|message_type |消息类型 0 文本消息  1 图片消息 2 语音消息  3 视频消息    4 文件消息 5 文本卡片消息 6 图文消息
7 图文消息（mpnews） 8 markdown消息 9 小程序通知消息 10 任务卡片消息 |varchar |null |是 |
|to_user |指定接收消息的成员 |varchar |null |是 |
|to_party |指定接收消息的部门 |varchar |null |是 |
|to_tag |指定接收消息的标签 |varchar |null |是 |
|message_json |消息体 |text |null |是 |
|push_range |消息范围 0 全部客户  1 指定客户 |varchar |null |是 |
|create_by | |varchar |null |是 |
|create_time |创建时间 |datetime |null |是 |
|update_by | |varchar |null |是 |
|update_time |更新时间 |datetime |null |是 |
|del_flag |0 未删除 1 已删除 |int |null |是 |
|invaliduser |无效用户 |varchar |null |是 |
|invalidparty |无效单位 |varchar |null |是 |
|invalidtag |无效标签 |varchar |null |是 |
|chat_id |群聊id |text |null |是 |

### we_msg_tlp(欢迎语模板表)
| 字段名 | 字段说明 | 字段类型 | 默认值 | 是否为空 |
|------ |------ |------ |------ |------ |
|id |id |bigint |null |不为空 |
|welcome_msg |欢迎语 |varchar |null |是 |
|media_id |素材的id |bigint |null |是 |
|del_flag |0:正常;2:删除; |tinyint |0 |是 |
|welcome_msg_tpl_type |欢迎语模板类型:1:员工欢迎语;2:部门员工欢迎语;3:客户群欢迎语 |tinyint |null |是 |
|create_by | |varchar |null |是 |
|create_time |创建时间 |datetime |null |是 |



### we_chat_collection(素材收藏表)
| 字段名 | 字段说明 | 字段类型 | 默认值 | 是否为空 |
|------ |------ |------ |------ |------ |
|collection_id |聊天工具 侧边栏栏 素材收藏 |bigint |null |不为空 |
|material_id |素材id |bigint |null |是 |
|user_id | |varchar |null |是 |
|create_by | |varchar |null |是 |
|create_time |创建时间 |datetime |null |是 |
|update_by | |varchar |null |是 |
|update_time |更新时间 |datetime |null |是 |

### we_chat_contact_mapping(聊天关系映射表)
| 字段名 | 字段说明 | 字段类型 | 默认值 | 是否为空 |
|------ |------ |------ |------ |------ |
|id |id |bigint |null |不为空 |
|from_id |发送人id |varchar |null |不为空 |
|receive_id |接收人id |varchar |null |是 |
|room_id |群聊id |varchar |null |是 |
|is_custom |接收人是否为客户 0-成员 1-客户 2-机器人 |tinyint |null |是 |

### we_chat_contact_msg(会话消息)
| 字段名 | 字段说明 | 字段类型 | 默认值 | 是否为空 |
|------ |------ |------ |------ |------ |
|id |id |bigint |null |不为空 |
|msg_id |消息id |varchar |null |不为空 |
|from_id |发送人id |varchar |null |不为空 |
|to_list |接收人id（列表） |text |null |不为空 |
|room_id |群聊id |varchar |null |是 |
|action |消息类型 |varchar |null |不为空 |
|msg_type |消息类型(如：文本，图片) |varchar |null |不为空 |
|msg_time |发送时间 |datetime |null |不为空 |
|seq |消息标识 |bigint |null |是 |
|contact |消息内容 |text |null |是 |
|is_external |是否为外部聊天 0 外部 1 内部 |tinyint |0 |不为空 |
|create_by |创建人 |varchar |0 |不为空 |
|create_time |创建时间 |datetime |CURRENT_TIMESTAMP |不为空 |
|update_by |修改人 |varchar |0 |是 |
|update_time |更新时间 |datetime |null |是 |
|del_flg |删除标识 0正常 1 删除 |tinyint |0 |不为空 |

### we_chat_item(素材菜单对应素材表)
| 字段名 | 字段说明 | 字段类型 | 默认值 | 是否为空 |
|------ |------ |------ |------ |------ |
|item_id | |bigint |null |不为空 |
|side_id |聊天工具栏id |bigint |null |是 |
|material_id |素材id |bigint |null |是 |
|create_by | |varchar |null |是 |
|create_time |创建时间 |datetime |null |是 |
|update_by | |varchar |null |是 |
|update_time |更新时间 |datetime |null |是 |

### we_chat_side(素材菜单表)
| 字段名 | 字段说明 | 字段类型 | 默认值 | 是否为空 |
|------ |------ |------ |------ |------ |
|side_id |主键id |bigint |null |不为空 |
|media_type |素材类型 0 图片（image）、1 语音（voice）、2 视频（video），3 普通文件(file) 4 文本 5 海报 |bigint |null |是 |
|side_name |聊天工具栏名称 |varchar |null |是 |
|total |已抓取素材数量 |int |null |是 |
|using |是否启用 0 启用 1 未启用 |int |0 |是 |
|create_by | |varchar |null |是 |
|create_time |创建时间 |datetime |null |是 |
|update_by | |varchar |null |是 |
|update_time |更新时间 |datetime |null |是 |
|del_flag |0  未删除 1 已删除 |int |0 |是 |


### we_allocate_customer(离职分配的客户列表)
| 字段名 | 字段说明 | 字段类型 | 默认值 | 是否为空 |
|------ |------ |------ |------ |------ |
|id |id |bigint |null |不为空 |
|takeover_userid |接替成员的userid |varchar |null |不为空 |
|external_userid |被分配的客户id |varchar |null |不为空 |
|allocate_time |分配时间 |datetime |null |不为空 |
|handover_userid |原跟进成员的userid |varchar |null |是 |
|status |接替状态， 1-接替完毕 2-等待接替 3-客户拒绝 4-接替成员客户达到上限 5-无接替记录 |tinyint |2 |是 |
|takeover_time |接替客户的时间，如果是等待接替状态，则为未来的自动接替时间 |datetime |null |是 |
|fail_reason |失败原因 |varchar |null |是 |

### we_allocate_group(分配的群租表)
| 字段名 | 字段说明 | 字段类型 | 默认值 | 是否为空 |
|------ |------ |------ |------ |------ |
|id |id |bigint |null |不为空 |
|chat_id |分配的群id |varchar |null |是 |
|new_owner |新群主 |varchar |null |是 |
|old_owner |原群主 |varchar |null |是 |
|allocate_time |分配时间 |datetime |null |是 |

### we_app(应用参数相关表)
| 字段名 | 字段说明 | 字段类型 | 默认值 | 是否为空 |
|------ |------ |------ |------ |------ |
|id |id |bigint |null |不为空 |
|agent_id |应用id |bigint |null |是 |
|agent_name |应用名称 |varchar |null |是 |
|agent_secret |应用密钥 |varchar |null |是 |
|description |应用描述 |varchar |null |是 |
|square_logo_url |应用图标 |varchar |null |是 |
|close |企业应用是否被停用(1:是;0:否) |tinyint |null |是 |
|redirect_domain |企业应用可信域名 |varchar |null |是 |
|report_location_flag |企业应用是否打开地理位置上报 0：不上报；1：进入会话上报； |tinyint |null |是 |
|isreportenter |是否上报用户进入应用事件。0：不接收；1：接收 |tinyint |null |是 |
|home_url |应用主页url |varchar |null |是 |
|app_type |应用类型(1:小程序;2:其他) |tinyint |2 |是 |
|create_time |创建时间 |datetime |null |是 |
|del_flag |删除标志（0代表存在 2代表删除） |char |0 |是 |
|status |帐号状态（0正常 1停用) |char |0 |是 |
|allow_partys | |varchar |null |是 |
|allow_userinfos | |varchar |null |是 |

### we_category(素材分类表)
| 字段名 | 字段说明 | 字段类型 | 默认值 | 是否为空 |
|------ |------ |------ |------ |------ |
|id |id |bigint |null |不为空 |
|media_type |0 图片（image）、1 语音（voice）、2 视频（video），3 普通文件(file) 4 文本 5 海报 |int |null |不为空 |
|name |分类名称 |varchar |null |是 |
|parent_id |父分类的id |bigint |0 |是 |
|create_by | |varchar |null |是 |
|create_time |创建时间 |datetime |null |是 |
|update_by | |varchar |null |是 |
|update_time |更新时间 |datetime |null |是 |
|del_flag |0  未删除 2 已删除 |char |0 |是 |


### we_keyword_group(关键词拉群任务表)
| 字段名 | 字段说明 | 字段类型 | 默认值 | 是否为空 |
|------ |------ |------ |------ |------ |
|task_id |关键词拉群任务主键 |bigint |null |不为空 |
|task_name |任务名称 |varchar |null |不为空 |
|group_code_id |群活码id |bigint |null |不为空 |
|welcome_msg |加群欢迎语 |varchar |null |不为空 |
|create_by |创建者 |varchar | |是 |
|create_time |创建时间 |datetime |null |是 |
|update_by |更新者 |varchar | |是 |
|update_time |更新时间 |datetime |null |是 |
|remark |备注 |varchar | |是 |
|del_flag |删除标记 0 未删除 1已删除 |char |0 |不为空 |
|keywords |关键词，以逗号分隔 |varchar |null |不为空 |

### we_msg_tlp_scope(模板使用人员范围表)
| 字段名 | 字段说明 | 字段类型 | 默认值 | 是否为空 |
|------ |------ |------ |------ |------ |
|id |id |bigint |null |不为空 |
|msg_tlp_id |模板id |bigint |null |是 |
|use_user_id |使用人id |bigint |null |是 |
|del_flag |0:正常;2:删除; |tinyint |0 |是 |

### we_poster(海报表)
| 字段名 | 字段说明 | 字段类型 | 默认值 | 是否为空 |
|------ |------ |------ |------ |------ |
|id |id |bigint |null |不为空 |
|del_flag |0 正常 1 删除 |tinyint |null |是 |
|create_by | |varchar |null |是 |
|create_time |创建时间 |timestamp |null |是 |
|update_by | |varchar |null |是 |
|update_time |更新时间 |timestamp |null |是 |
|title |图片标题 |varchar |null |是 |
|background_img_path |背景图层图片地址 |varchar |null |是 |
|sample_img_path |示例图 |varchar |null |是 |
|type |类型 1 通用海报 |bigint |null |是 |
|width |背景图片宽 |int |null |是 |
|height |背景图片高 |int |null |是 |
|media_type | |varchar |null |是 |
|category_id |分类id |bigint |null |是 |

### we_poster_font(海报字体相关表)
| 字段名 | 字段说明 | 字段类型 | 默认值 | 是否为空 |
|------ |------ |------ |------ |------ |
|id |id |bigint |null |不为空 |
|font_name |字体名称 |varchar |null |是 |
|font_url |字体网络地址 |varchar |null |是 |
|order |排序 |tinyint |null |是 |
|del_flag | |tinyint |null |是 |
|create_by | |varchar |null |是 |
|create_time |创建时间 |timestamp |null |是 |
|update_by | |varchar |null |是 |
|update_time |更新时间 |timestamp |null |是 |
|media_type | |int |null |是 |
|category_id | |bigint |null |是 |

### we_poster_subassembly(海报属性相关表)
| 字段名 | 字段说明 | 字段类型 | 默认值 | 是否为空 |
|------ |------ |------ |------ |------ |
|id |id |bigint |null |不为空 |
|del_flag | |tinyint |null |是 |
|create_by | |varchar |null |是 |
|create_time |创建时间 |timestamp |null |是 |
|update_by | |varchar |null |是 |
|update_time |更新时间 |timestamp |null |是 |
|poster_id |海报id |bigint |null |是 |
|left | |int |null |是 |
|top | |int |null |是 |
|width |宽 |int |null |是 |
|height |高 |int |null |是 |
|type |1 文本 2 图片 3 二维码 |int |null |是 |
|font_size |字体大小 |int |null |是 |
|font_color |字体颜色 |varchar |null |是 |
|img_path |图片地址 |varchar |null |是 |
|font_id |字体id |bigint |null |是 |
|font_text_align |水平对齐 |int |null |是 |
|content |内容 |varchar |null |是 |
|vertical_type |垂直对齐方式 |tinyint |null |是 |
|word_space |字间距 |int |null |是 |
|line_space |行间距 |int |null |是 |
|alpha |透明度[0,255] |int |null |是 |
|rotate |旋转角度（顺时针） |int |null |是 |
|font_style |字体类型 0 通常 1 粗体 2 斜体 |tinyint |null |是 |
|order |排序 顺序 |int |null |是 |


### we_pres_tag_group(老客户标签建群表)
| 字段名 | 字段说明 | 字段类型 | 默认值 | 是否为空 |
|------ |------ |------ |------ |------ |
|task_id |老客户标签建群任务id |bigint |null |不为空 |
|task_name |任务名称 |varchar |null |不为空 |
|send_type |发送方式 0: 企业群发 1：个人群发 |tinyint |0 |是 |
|create_by |创建人 |varchar |null |是 |
|create_time |创建时间 |datetime |null |是 |
|update_by |更新人 |varchar |null |是 |
|update_time |更新时间 |datetime |null |是 |
|group_code_id |群活码id |bigint |null |不为空 |
|send_scope |发送范围 0: 全部客户 1：部分客户 |tinyint |0 |不为空 |
|send_gender |发送性别 0: 全部 1： 男 2： 女 3：未知 |tinyint |0 |不为空 |
|cus_begin_time |目标客户被添加起始时间 |datetime |null |是 |
|cus_end_time |目标客户被添加结束时间 |datetime |null |是 |
|welcome_msg |加群引导语 |varchar |null |不为空 |
|msgid |企业群发消息的id |varchar |null |是 |
|del_flag |逻辑删除字段， 0:未删除 1:已删除 |tinyint |0 |是 |

### we_pres_tag_group_scope(老客标签建群使用范围表)
| 字段名 | 字段说明 | 字段类型 | 默认值 | 是否为空 |
|------ |------ |------ |------ |------ |
|task_id |老客户标签建群任务id |bigint |null |不为空 |
|we_user_id |员工id |varchar |null |不为空 |
|is_done |是否已处理 |smallint |0 |不为空 |

### we_pres_tag_group_stat(老客标签建群客户统计表)
| 字段名 | 字段说明 | 字段类型 | 默认值 | 是否为空 |
|------ |------ |------ |------ |------ |
|task_id |老客标签建群任务id |bigint |null |不为空 |
|external_userid |客户id |varchar |null |不为空 |

### we_pres_tag_group_tag(老客标签建群标签关联表)
| 字段名 | 字段说明 | 字段类型 | 默认值 | 是否为空 |
|------ |------ |------ |------ |------ |
|task_id |老客户标签建群任务id |bigint |null |不为空 |
|tag_id |标签id |varchar |null |不为空 |


### we_sensitive(敏感词设置表)
| 字段名 | 字段说明 | 字段类型 | 默认值 | 是否为空 |
|------ |------ |------ |------ |------ |
|id |id |bigint |null |不为空 |
|strategy_name |策略名称 |varchar |null |不为空 |
|pattern_words |匹配词 |text |null |不为空 |
|audit_user_id |审计人id |varchar |null |不为空 |
|audit_user_name |审计人 |varchar |null |不为空 |
|alert_flag |消息通知,1 开启 0 关闭 |tinyint |1 |不为空 |
|del_flag |删除标识，1 已删除 0 未删除 |tinyint |0 |不为空 |
|create_by |创建人 |varchar |null |是 |
|create_time |创建时间 |datetime |null |是 |
|update_by |更新人 |varchar |null |是 |
|update_time |更新时间 |datetime |null |是 |

### we_sensitive_act(敏感行为表)
| 字段名 | 字段说明 | 字段类型 | 默认值 | 是否为空 |
|------ |------ |------ |------ |------ |
|id |id |bigint |null |不为空 |
|act_name |敏感行为名称 |varchar |null |不为空 |
|order_num |排序字段 |int |0 |不为空 |
|enable_flag |记录敏感行为,1 开启 0 关闭 |tinyint |1 |不为空 |
|del_flag |删除标识，1 已删除 0 未删除 |tinyint |0 |不为空 |
|create_by |创建人 |varchar |null |是 |
|create_time |创建时间 |datetime |null |是 |
|update_by |更新人 |varchar |null |是 |
|update_time |更新时间 |datetime |null |是 |

### we_sensitive_act_hit(敏感行为记录表)
| 字段名 | 字段说明 | 字段类型 | 默认值 | 是否为空 |
|------ |------ |------ |------ |------ |
|id |id |bigint |null |不为空 |
|operator_id |敏感行为操作人id |varchar |null |不为空 |
|operator |敏感行为操作人 |varchar |null |不为空 |
|operate_target_id |敏感行为操作对象id |varchar |null |不为空 |
|operate_target |敏感行为操作对象 |varchar |null |不为空 |
|sensitive_act_id |敏感行为id |bigint |null |不为空 |
|sensitive_act |敏感行为名称 |varchar |null |不为空 |
|del_flag |删除标识，1 已删除 0 未删除 |tinyint |0 |不为空 |
|create_by |创建人 |varchar |null |是 |
|create_time |创建时间 |datetime |null |是 |
|update_by |更新人 |varchar |null |是 |
|update_time |更新时间 |datetime |null |是 |

### we_sensitive_audit_scope(敏感词审计范围表)
| 字段名 | 字段说明 | 字段类型 | 默认值 | 是否为空 |
|------ |------ |------ |------ |------ |
|id |id |bigint |null |不为空 |
|sensitive_id |敏感词表主键 |bigint |null |不为空 |
|scope_type |审计范围类型, 1 组织机构 2 成员 |tinyint |null |不为空 |
|audit_scope_id |审计对象id |varchar |null |不为空 |
|audit_scope_name |审计对象名称 |varchar |null |不为空 |


### we_task_fission(任务宝表)
| 字段名 | 字段说明 | 字段类型 | 默认值 | 是否为空 |
|------ |------ |------ |------ |------ |
|id |id |bigint |null |不为空 |
|fission_type |活动类型，1 任务宝 2 群裂变 |int |1 |不为空 |
|task_name |任务活动名称 |varchar | |不为空 |
|fiss_info |裂变引导语 |varchar |null |是 |
|fiss_num |裂变客户数量 |int |1 |不为空 |
|start_time |活动开始时间 |datetime |null |不为空 |
|over_time |活动结束时间 |datetime |null |不为空 |
|customer_tag_id |客户标签id列表，当为全部时保存为all |mediumtext |null |不为空 |
|customer_tag |客户标签名称列表，为all是可为空 |mediumtext |null |是 |
|posters_id |海报id |bigint |null |是 |
|posters_url |裂变海报路径 |varchar | |不为空 |
|fission_target_id |任务裂变目标员工/群裂变id |varchar | |不为空 |
|fission_target |任务裂变目标员工姓名/群裂变二维码地址 |varchar | |不为空 |
|fiss_qrcode |任务裂变目标二维码 |varchar | |不为空 |
|reward_url |兑奖链接 |varchar | |不为空 |
|reward_image_url |兑奖链接图片 |varchar | |不为空 |
|reward_rule |兑奖规则 |mediumtext |null |是 |
|fiss_status |任务裂变活动状态，1 进行中 2 已结束 |tinyint |1 |不为空 |
|welcome_msg |新客欢迎语 |mediumtext |null |是 |
|create_by |创建人 |varchar |null |是 |
|create_time |创建时间 |datetime |null |是 |
|update_by |更新人 |varchar |null |是 |
|update_time |更新时间 |datetime |null |是 |

### we_task_fission_complete_record(裂变任务完成记录表)
| 字段名 | 字段说明 | 字段类型 | 默认值 | 是否为空 |
|------ |------ |------ |------ |------ |
|id |id |bigint |null |不为空 |
|task_fission_id |任务裂变表id |bigint |null |不为空 |
|fission_record_id |任务裂变记录表id |bigint |null |不为空 |
|customer_id |裂变客户id |varchar |NULL |不为空 |
|customer_name |裂变客户姓名 |varchar |null |是 |
|create_time |创建时间 |datetime |null |是 |
|status |状态 0 有效 1无效 |tinyint |1 |不为空 |
|customer_avatar |客户头像 |varchar |null |是 |

### we_task_fission_record(裂变任务记录表)
| 字段名 | 字段说明 | 字段类型 | 默认值 | 是否为空 |
|------ |------ |------ |------ |------ |
|id |id |bigint |null |不为空 |
|task_fission_id |任务裂变表id |bigint |null |不为空 |
|customer_id |裂变任务客户id |varchar |NULL |不为空 |
|customer_name |裂变任务客户姓名 |varchar |null |是 |
|fiss_num |裂变客户数量 |int |0 |不为空 |
|qr_code |二维码链接 |varchar |NULL |不为空 |
|create_time |创建时间 |datetime |null |是 |
|complete_time |完成时间 |datetime |null |是 |
|poster |海报链接 |varchar |null |是 |

### we_task_fission_reward(任务裂变奖励表)
| 字段名 | 字段说明 | 字段类型 | 默认值 | 是否为空 |
|------ |------ |------ |------ |------ |
|id |id |bigint |null |不为空 |
|task_fission_id |任务裂变id |bigint |null |不为空 |
|reward_code |兑奖码 |varchar |NULL |不为空 |
|reward_code_status |兑奖码状态，0 未使用 1 已使用 |tinyint |0 |不为空 |
|reward_user_id |兑奖用户id |varchar |null |是 |
|reward_user |兑奖人姓名 |varchar |null |是 |
|create_time |创建时间 |datetime |null |是 |
|create_by |创建者 |varchar | |是 |
|update_by |更新者 |varchar | |是 |
|update_time |更新时间 |datetime |null |是 |

### we_task_fission_staff(裂变任务员工列表)
| 字段名 | 字段说明 | 字段类型 | 默认值 | 是否为空 |
|------ |------ |------ |------ |------ |
|id |id |bigint |null |不为空 |
|task_fission_id |任务裂变表id |bigint |null |不为空 |
|staff_type |员工或机构，1 组织机构 2 成员 3 全部 |tinyint |null |不为空 |
|staff_id |员工或组织机构id,为全部时为空 |varchar |null |是 |
|staff_name |员工或组织机构姓名，类型为全部时，为空 |varchar |null |是 |




